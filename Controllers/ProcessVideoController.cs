﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using webAPI.MainClass;
using webAPI.Model;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Blob.Protocol;

using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using System.Threading;
using webAPI.DetectClasses;
using Microsoft.AspNetCore.Hosting;
using System.Data;
using Microsoft.AspNetCore.Cors;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using MediaServices = webAPI.Services.MediaServices;

//using Microsoft.WindowsAzure.MediaServices.Client;

using Microsoft.WindowsAzure.Storage.RetryPolicies;
using webAPI.Services;

//using Microsoft.WindowsAzure.MediaServices.Client.DynamicEncryption;
//using Microsoft.WindowsAzure.MediaServices.Client.ContentKeyAuthorization;


namespace webAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowOrigin")]
    public class ProcessVideoController : Controller
    {
        Dictionary<string, string> jsonResult;
        private Mp4Gen mp4Gen = new Mp4Gen();
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        public string Get8CharacterRandomString()
        {
            string path = Path.GetRandomFileName();
            path = path.Replace(".", ""); // Remove period.
            return path.Substring(0, 8);  // Return 8 character string
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost("get/graph_data")]
        public JsonResult getGraphData([FromBody]  mdlProcessVideo form) {
            var doctorID = form.doctorID;
            SqlConnection myConnection = new SqlConnection(MClass.strConn);

            SqlDataReader reader;
            myConnection.Open();
            string sql = "SELECT top 20 [objectName] ,[timeAppear], [xBound], [yBound] FROM [dbo].[objectBoundTB] where [videoID] = " + doctorID;

            SqlCommand cmd = new SqlCommand(sql, myConnection);
            reader = cmd.ExecuteReader();
            Dictionary<int, object> json = new Dictionary<int, object>();
            int i = 1;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> jsonResult = new Dictionary<string, object>();
                    jsonResult["objectName"] = reader.GetValue(0).ToString();
                    jsonResult["timeAppear"] = reader.GetValue(1).ToString();
                    jsonResult["xBound"] = reader.GetValue(2).ToString();
                    jsonResult["yBound"] = reader.GetValue(3).ToString(); 

                    json.Add(i, jsonResult);
                    i++;
                }
            }

            cmd.Dispose();


            return Json(json);

        }

        [HttpPost("insert/form")]
        [DisableRequestSizeLimit]
        public JsonResult UploadFile() {
            var file = HttpContext.Request.Form.Files["file"];
            var wait = upload(file);
            wait.Wait();

            jsonResult = new Dictionary<string, string>();
            jsonResult["Status"] = "1";// uploaded.ToString();
            return Json(jsonResult);
        }
        public async Task<string> upload(IFormFile file)
        {
            
            var httpRequest = HttpContext.Request.Form.Files["file"];
            var doctorID = HttpContext.Request.Form["doctorID"][0];
            var title  = HttpContext.Request.Form["title"][0];        
            
            SqlConnection myConnection = new SqlConnection(MClass.strConn);
            myConnection.Open();             
            
            StreamContent streamcontent = new StreamContent(file.OpenReadStream());

            string tenantDomain = "azuresibelanddawn.onmicrosoft.com";
            string restApiUrl = "https://mtoolsmediaservices.restv2.southcentralus.media.azure.net/api/";
            string clientId = "585529e4-c763-4aa5-abbd-76a1bacf9172";
            string clientSecret = "cRp4fER/LqNKw01Dmog8LgWYN89w9PWSxyW+XfMyDEQ=";

            string fname = Get8CharacterRandomString();

            MediaServices mediaService = new MediaServices(tenantDomain, restApiUrl, clientId, clientSecret);
            await mediaService.InitializeAccessTokenAsync();

            string filename = fname + file.FileName;

            string accessPolicyId = await mediaService.GenerateAccessPolicy("TestAccessPolicy1", 100, 2);
            Asset asset = await mediaService.GenerateAsset(filename, "apitools");
            Locator locator = await mediaService.GenerateLocator(accessPolicyId, asset.Id, DateTime.Now.AddMinutes(-5), 1);            

            // upload the file to azure and generate the asset's file info
            await mediaService.UploadBlobToLocator(streamcontent, locator, filename);
            await mediaService.GenerateFileInfo(asset.Id);

            // run an encoding job on the uploaded asset
            string mediaProcessorId = await mediaService.GetMediaProcessorId("Media Encoder Standard");
            var result = await mediaService.CreateJob(filename, asset.Uri, mediaProcessorId, "H264 Single Bitrate 720p");
            var waiting = await mediaService.WaitForJobToFinish(result);

            string outputAssetId = await mediaService.GetAsset(filename);

            string outputaccessPolicyId = await mediaService.GenerateAccessPolicy("Streaming policy", 100000, 1);

            string publishUrl = await mediaService.GenerateStreamingLocator(outputaccessPolicyId, outputAssetId, DateTime.Now.AddMinutes(-5), 2, filename);

            string sql = "INSERT INTO [dbo].[doctorVideoTB] ([doctorID] , [VIDEOTOTALLENGTH], [title], [videoResources] )  VALUES (" + doctorID + ", " + httpRequest.Length + ", '" + title + "', '" + publishUrl + "')";

            SqlCommand cmd = new SqlCommand(sql, myConnection);
            cmd.ExecuteNonQuery();
            cmd.Dispose();

            return "Success";
        }
        public async Task<IActionResult> PostVideo(IFormFile file)
        {
            var stream = file.OpenReadStream();
            var name = file.FileName;            

            UploadFileAsBlob(stream, name).GetAwaiter().GetResult();
            return null; //null just to make error free
        }
        /*
        public void CreateBLOBContainer(string fileName, Stream stream) {
            try {
                string result = string.Empty;
                CloudMediaContext mediaContext;
                mediaContext = new CloudMediaContext("", "");
                AssetCreationOptions assetOptions = new AssetCreationOptions();
                IAsset asset = mediaContext.Assets.Create("", assetOptions.None);
                return asset.Id;
                CloudMediaContext mediaCloud = new CloudMediaContext("account", "key");
                AssetCreationOptions assetOptions = new AssetCreationOptions();
                var asset = mediaCloud.Assets.Create(fileName, assetOptions);
                var assetFile = asset.AssetFiles.Create();
                var access
            }
            catch (Exception) {
                throw;
            }
        }*/        

        public async Task<string> UploadFileAsBlob(Stream stream, string filename)
        {            
            var httpClient = new HttpClient { BaseAddress = new Uri("restApiUrl") };
            httpClient.DefaultRequestHeaders.Add("", "2.15");
            httpClient.DefaultRequestHeaders.Add("x-ms-version", "2.15");
            httpClient.DefaultRequestHeaders.Add("DataServiceVersion", "3.0");
            httpClient.DefaultRequestHeaders.Add("MaxDataServiceVersion", "3.0");
            httpClient.DefaultRequestHeaders.Add("Accept", "application/json;odata=verbose");
            var body = $"resource={HttpUtility.UrlEncode("https://rest.media.azure.net")}&client_id={""}&client_secret={HttpUtility.UrlEncode("")}&grant_type=client_credentials";
            var httpContent = new StringContent(body, Encoding.UTF8, "application/x-www-form-urlencoded");
            var response = await httpClient.PostAsync($"https://login.microsoftonline.com/{"_tenantDomain"}/oauth2/token", httpContent);
            if (!response.IsSuccessStatusCode) throw new Exception();
            var resultBody = await response.Content.ReadAsStringAsync();
            var obj = JObject.Parse(resultBody);

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", obj["access_token"].ToString());

            string tenantDomain = "";
            string restApiUrl = "";
            string clientId = "";
            string clientSecret = "";
            MediaServices mediaService = new MediaServices(tenantDomain, restApiUrl, clientId, clientSecret);
            await mediaService.InitializeAccessTokenAsync();

            var bod = new
            {
                Name = "name",
                Options = 0,
                StorageAccountName = "storageAccountName"
            };

            var bodyContent = JsonConvert.SerializeObject(body);
            HttpResponseMessage responses = await httpClient.PostAsync("Assets", new StringContent(bodyContent, Encoding.UTF8, "application/json"));
            string responseContent = await response.Content.ReadAsStringAsync();

            var objects = JObject.Parse(responseContent);
            var Id = objects["d"]["Id"].ToString();
            var Uril = objects["d"]["__metadata"]["uri"].ToString();




            var storageaAccountName = new CloudStorageAccount(new StorageCredentials(MClass.storageAccountName, MClass.storageAccountKey), true);

            var blobClient = storageaAccountName.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("tool-container");
            var contain = container.CreateIfNotExistsAsync();
            contain.Wait();
            var permission = container.SetPermissionsAsync(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });
            permission.Wait();

            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

            await blockBlob.UploadFromStreamAsync(stream);

            stream.Dispose();
            return blockBlob?.Uri.ToString();
            
        }

        // POST api/<controller>
        [HttpPost("insert/data")]
        public JsonResult InsertVideo([FromBody]  mdlProcessVideo form)
        {
            try {
                var path = form.videoResources;
                var fileName = Path.GetFileName(path);
                string sql = "INSERT INTO [dbo].[doctorVideoTB] ([doctorID]  ,[videoResources]  )  VALUES (" + form.doctorID + ", '" + fileName+"')";

                SqlConnection myConnection = new SqlConnection(MClass.strConn);
                myConnection.Open();

                SqlCommand cmd = new SqlCommand(sql, myConnection);
                cmd.ExecuteNonQuery();
                cmd.Dispose();

                //Boolean uploaded = uploadingVideo(form.videoResources);
                var wc = new System.Net.WebClient();              
                
                wc.DownloadFile(form.videoResources, fileName);
                Boolean uploaded = uploadingVideoStreamAsync(fileName);
                jsonResult = new Dictionary<string, string>();
                jsonResult["Status"] = "1";// uploaded.ToString();
                return Json(jsonResult);
            }
            catch(Exception ex) {
                jsonResult = new Dictionary<string, string>();
                //jsonResult["Status"] = "0";
                jsonResult["errMsg"] = ex.ToString();
                return Json(jsonResult);
            }

        }

        // POST api/<controller>
        [HttpPost("download/blob")]
        public JsonResult downloadBLOB([FromBody]  mdlProcessVideo form)
        {


            var storageaAccountName = new CloudStorageAccount(new StorageCredentials(MClass.storageAccountName, MClass.storageAccountKey), true);

            var blobClient = storageaAccountName.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("tool-container");
            var contain = container.CreateIfNotExistsAsync();
            contain.Wait();
            while (container == null)
            {
                System.Threading.Thread.Sleep(1000);
            }



            //  var blob = container.ListBlobs();

            //Boolean uploaded = uploadingVideo(form.videoResources);
            var downloaded = DownloadFileFromBlobAsync(form.videoResources).GetAwaiter().GetResult();

            Dictionary<string, string> jsonResult = new Dictionary<string, string>();
            jsonResult["status"] = downloaded.ToString();
            return Json(downloaded);


        }


        public static void downLoadBlobs(IEnumerable<IListBlobItem> blobs)
        {
            foreach (var blob in blobs)
            {
                if (blob is CloudBlockBlob blobBlock)
                { blobBlock.DownloadToFileAsync(blobBlock.Name, FileMode.Create); }
                else if (blob is CloudBlobDirectory blobDirectory)
                {
                    Directory.CreateDirectory(blobDirectory.Prefix);
                    //downLoadBlobs(blobDirectory.ListBlobsSegmentedAsync());
                }
            }
        }

        public Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container;

        public async void blobConnect() {
            var storageCredentials = new StorageCredentials(MClass.storageAccountName, MClass.storageAccountKey);
            var storageaAccountName = new CloudStorageAccount(storageCredentials, true);

            var blobClient = storageaAccountName.CreateCloudBlobClient();
            container = blobClient.GetContainerReference("tool-container");
            await container.CreateIfNotExistsAsync();
        }

        public Boolean uploadingVideo(string filePath)
        {
            
            var storageCredentials = new StorageCredentials(MClass.storageAccountName, MClass.storageAccountKey);
            var storageaAccountName = new CloudStorageAccount(storageCredentials, true);

            var blobClient = storageaAccountName.CreateCloudBlobClient();
            container = blobClient.GetContainerReference("tool-container");
            var contain = container.CreateIfNotExistsAsync();
            contain.Wait();
            while (container == null) {
                System.Threading.Thread.Sleep(1000);
            }            

            var permission = container.SetPermissionsAsync(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });
            permission.Wait();

            var blob = container.GetBlockBlobReference(filePath);
            var task = blob.UploadFromFileAsync("");
            task.Wait();

            
            return true;
        }

        public Boolean uploadingVideoStreamAsync(string filePath)
        {

            var storageaAccountName = new CloudStorageAccount(new StorageCredentials(MClass.storageAccountName, MClass.storageAccountKey), true);

            var blobClient = storageaAccountName.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("tool-container");
            var contain = container.CreateIfNotExistsAsync();
            contain.Wait();
            var permission = container.SetPermissionsAsync(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });
            permission.Wait();

            var blob = container.GetBlockBlobReference(filePath);
            /*
            using (var fileStream = System.IO.File.OpenRead(filePath))
            {
                blob.UploadFromStreamAsync(fileStream);
            }
            */
            
            var task = blob.UploadFromFileAsync(filePath);
            task.Wait();
            System.IO.File.Delete(filePath);
            return true;
        }
        public async Task<byte[]> DownloadFileFromBlobAsync(string blobReferenceKey)
        {
            var storageaAccountName = new CloudStorageAccount(new StorageCredentials(MClass.storageAccountName, MClass.storageAccountKey), true);

            var blobClient = storageaAccountName.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("tool-container");
            await container.CreateIfNotExistsAsync();
            await container.SetPermissionsAsync(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });

            var blob = container.GetBlockBlobReference("videotest.mp4");

            using (var ms = new MemoryStream())
            {
                bool x = await blob.ExistsAsync();
                if (x)
                {
                    await blob.DownloadToStreamAsync(ms);
                }
                return ms.ToArray();
            }
        }
        [HttpPost("processed/video")]
        public JsonResult ProcessedVideo([FromBody]  mdlProcessVideo form)
        {

            SqlConnection myConnection = new SqlConnection(MClass.strConn);
            myConnection.Open();
            string sql = "UPDATE [dbo].[doctorVideoTB] set ";
            sql = sql + "[VideoStartTime] = '" + DateTime.Now + "'";
            sql = sql + "[processedVideoFileURL] = '" + DateTime.Now + "'";
            sql = sql + "[processDate] = '" + DateTime.Now + "'";
            sql = sql + "where  WHERE vSerial = " + form.vSerial; ;


            SqlCommand cmd = new SqlCommand(sql, myConnection);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            Dictionary<string, string> jsonResult = new Dictionary<string, string>();
            jsonResult["status"] = "1";
            return Json(jsonResult);


        }

        [HttpPost("video/object")]
        public JsonResult VideoObject([FromBody]  mdlVideoObjects form)
        {

            SqlConnection myConnection = new SqlConnection(MClass.strConn);
            myConnection.Open();
            string sql = "INSERT INTO [dbo].[objectBoundTB] ([videoID]  ,[objectName], [timeAppear],[xBound],[yBound] )  VALUES (" + form.oVideoID + ",'" + form.ObjectText + "','" + form.oAppearnceTime + "' ," + form.xBoundBox + " ," + form.yBoundBox + ")";

            SqlCommand cmd = new SqlCommand(sql, myConnection);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            Dictionary<string, string> jsonResult = new Dictionary<string, string>();
            jsonResult["status"] = "1";
            return Json(jsonResult);



        }

        [HttpPost("video/search")]
        public JsonResult VideoSearch([FromBody]  mdlProcessVideo form)
        {
          try
            { 
                SqlConnection myConnection = new SqlConnection(MClass.strConn);
                SqlConnection myConnection2 = new SqlConnection(MClass.strConn);
                
                SqlDataReader reader;
                myConnection.Open();
                string sql = "SELECT [vSerial] ,[doctorID],[videoResources] ,[processedVideoFileURL],[processDate] ,[timeToProcess],[video_start_time] , [video_total_length],[metaDataBlockID] ,[IdentifiedObjectsInThisVideo] ,[VideoStartTime] ,[VideoTotalLength], [title], [CREATEDATE] FROM [dbo].[doctorVideoTB] where [doctorID] = " + form.doctorID;

                SqlCommand cmd = new SqlCommand(sql, myConnection);
                reader = cmd.ExecuteReader();

                int i = 1;
                Dictionary<int, object> json = new Dictionary<int, object>();
                if (reader.HasRows) {
                    while (reader.Read())
                    {
                        Dictionary<string, object> jsonResult = new Dictionary<string, object>();
                        jsonResult["_comment"] = "Video Processing API Azure";
                        jsonResult["status"] = "success";
                        jsonResult["videoID"] = reader.GetValue(0).ToString();
                        jsonResult["doctor_ID"] = reader.GetValue(1);
                        jsonResult["videoResources"] = reader.GetValue(2);

                        jsonResult["processed_video_file_url"] = reader.GetValue(3).ToString();
                        jsonResult["processed_date_epoch_time"] = reader.GetValue(4).ToString();
                        jsonResult["time_to_process_ms"] = reader.GetValue(5).ToString();

                        jsonResult["video_start_time"] = reader.GetValue(6).ToString();
                        jsonResult["video_total_length"] = reader.GetValue(11).ToString();
                        jsonResult["title"] = reader.GetValue(12).ToString();
                        jsonResult["created"] = reader.GetValue(13).ToString();

                        myConnection2.Open();
                        SqlDataAdapter a = new SqlDataAdapter("SELECT [Serial] ,[videoID] ,[objectName]  ,[timeAppear] ,[xBound]  ,[yBound]  FROM [dbo].[objectBoundTB] where videoID = 712 ", myConnection2);
                        DataTable t = new DataTable();
                        a.Fill(t);
                        var boundTb = t;
                        jsonResult["objectBound"] = boundTb;
                        myConnection2.Close();
                   
                        json.Add(i, jsonResult);
                        i++;
                    }
                }

                cmd.Dispose();


                return Json(json);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> jsonResult = new Dictionary<string, object>();
                jsonResult["Error"] = ex.ToString();
                return Json(jsonResult);
            }



        }

        //------------ Detect Object------
        [HttpPost("detect/object")]
        public JsonResult DetectObject([FromBody]  videoAttribute form)
        {
           // var folderName = Path.Combine("Resources", "Images");
           // var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), "vidFolder");

           // string physicalWebRootPath = Microsoft.AspNetCore.Hosting.Server.MapPath("~/");
            Thread thread = new Thread(() =>
            {
               
                mp4Gen.DetectTools(form.filePath, form.saveFilePath);
               // mp4Gen.DetectTools(form.filePath, form.saveFilePath);
            });
            thread.SetApartmentState(ApartmentState.MTA);
            thread.Priority = ThreadPriority.AboveNormal;
            thread.DisableComObjectEagerCleanup();
            //saveFilePath = "";
            thread.Start();
            //Boolean uploaded = uploadingVideo(form.saveFilePath);
            Dictionary<string, string> jsonResult = new Dictionary<string, string>();
            jsonResult["status"] = "1"; //uploaded.ToString();
            return Json(jsonResult);



        }
        //----------------------------

        /*    [HttpPost("video/Object")]
            public JsonResult VideoObject([FromBody]  mdlVideoObjects form)
            {

                SqlConnection myConnection = new SqlConnection(MClass.strConn);
                myConnection.Open();
               /* string sql = "UPDATE [dbo].[doctorVideoTB] set ";
                sql = sql + "[VideoStartTime] = '" + DateTime.Now + "'";
                sql = sql + "[processDate] = '" + DateTime.Now + "'";
                sql = sql + "where  WHERE vSerial = " + form.vSerial; ;


                SqlCommand cmd = new SqlCommand(sql, myConnection);
                cmd.ExecuteNonQuery();
                cmd.Dispose();

                string sqlInsertObj = "INSERT INTO [dbo].[ObjectsTB] ([oVideoID] ,[ObjectText] ,[oAppearnceTime] ,[oBoundBox])  VALUES ( ";
                sqlInsertObj = sqlInsertObj +    " +" + form.oVideoID +", " + form.ObjectText+ ", '" + DateTime.Now + "' , '" + form.oBoundBox + "'  )";
                SqlCommand cmdObject = new SqlCommand(sqlInsertObj, myConnection);
                cmdObject.ExecuteNonQuery();
                cmdObject.Dispose();

                Dictionary<string, string> jsonResult = new Dictionary<string, string>();
                jsonResult["status"] = "1";
                return Json(jsonResult);

        
    }*/

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
