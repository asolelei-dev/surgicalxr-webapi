﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webAPI.Model
{
    public class mdlProcessVideo
    {
        public int? vSerial { get; set; }
        public int? doctorID { get; set; }
        public string videoResources { get; set; }
        public string processedVideoFileURL { get; set; }
        public DateTime processDate { get; set; }
        public DateTime timeToProcess { get; set; }
        public int? metaDataBlockID { get; set; }
        public int? IdentifiedObjectsInThisVideo { get; set; }
        public DateTime VideoStartTime { get; set; }
        public DateTime VideoTotalLength { get; set; }

    }
}
