﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webAPI.Model
{
    public class mdlVideoObjects
    {
        public int? vSerial { get; set; }
        public int? oSerial { get; set; }
        public int? oVideoID { get; set; }
        public string ObjectText { get; set; }
        public DateTime oAppearnceTime { get; set; }
        public double xBoundBox { get; set; }
        public double yBoundBox { get; set; }

    }
}
