﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webAPI.DetectClasses
{
    class CVRegions
    {
        public string id { set; get; }
        public string iteration { set; get; }
        public string project { set; get; }
        public string created { set; get;}
        public Predictions[] predictions { set; get; }

        public class Predictions
        {
            public double probability { set; get; }
            public string tagId { set; get; }
            public string tagName { set; get; }
            public BoundingBox boundingBox { set; get; }

            public class BoundingBox
            {
                public float left { set; get; }
                public float top { set; get; }
                public float width { set; get; }
                public float height { set; get; }
            }
        }
    }
    
}
