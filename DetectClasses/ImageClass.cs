﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webAPI.DetectClasses
{
    public class ImageClass
    {
        public Uri ImageUri { get; private set; }

        public string Label { get; set; }

        public ImageClass(string location)
        {
            if (File.Exists(location))
            {
                ImageUri = new Uri(location);
            }
            Label = location;
        }
    }
}
