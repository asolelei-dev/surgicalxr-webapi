﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Globalization;
using System.Xml.Linq;
using Emgu.CV;
using Newtonsoft.Json;

namespace webAPI.DetectClasses
{
    class CustomVision
    {
        int itr = 0;
        private static string _URL;
        private XDocument xDocument;
        private string predictionKey;
        public CustomVision()
        {
            //xDocument = XDocument.Load("Settings.xml");
            _URL = "https://southcentralus.api.cognitive.microsoft.com/customvision/v3.0/Prediction/de2becfa-f486-4efa-a6f6-5a4dd235c7f0/detect/iterations/Iteration4/image";//xDocument.Descendants("url").FirstOrDefault().Value;
            predictionKey = "4f4ce055e91846ac8744b95c639f539e";// xDocument.Descendants("prediction-key").FirstOrDefault().Value;
        }
        [MTAThread]
        public async Task<CVRegions> processImage(Mat frame)
        {
            HttpClient client = new HttpClient();
            CVRegions regions = new CVRegions();
            client.DefaultRequestHeaders.Add("Prediction-Key", predictionKey);
            try

            {
               // itr = 0;
                using (var content =
                    new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                {
                    //itr = 16548;
                MemoryStream memoryStream = new MemoryStream();
                    itr = itr + 1; 
                    //if (itr >= 16548)
                   // { 
                      //frame.Bitmap.Save(@"C:\OperationSamples\Robocag2_ch1\rc" + itr + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                   // }
                                    
                     frame.Bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                     content.Add(new StreamContent(new MemoryStream(memoryStream.ToArray())), "file", "Capture.jpeg");


                     using (
                         var message = await client.PostAsync(_URL, content))
                     {
                         if (message.IsSuccessStatusCode)
                         {
                             var input = await message.Content.ReadAsStringAsync();

                             regions = JsonConvert.DeserializeObject<CVRegions>(input);
                         }

                     }

                     memoryStream.Dispose();
                    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                client.CancelPendingRequests();
            }
            finally
            {
                client.Dispose();
            }
                
            return regions;
        }
    }
}
