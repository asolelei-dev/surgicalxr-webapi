﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Newtonsoft.Json;
using webAPI;

namespace webAPI.DetectClasses
{
    class Mp4Gen
    {
        public delegate void ProgressUpdate(double value, int totalFrames, int processedFrames);

        private VideoWriter writer;
        public delegate void ProgressFinished();
        public event ProgressUpdate OnProgressUpdate;
        public event ProgressFinished OnProgressFinished;
        private double totalFrames;
        private TagNamesExcel tagNamesExcel;
        private Queue<MyQueue> requsetQueues = new Queue<MyQueue>();

        public Mp4Gen()
        {
           // tagNamesExcel = new TagNamesExcel();
        }

        [STAThread]
        public void makeVideo()
        {
            for (int i = 0; i < totalFrames; i++)
            {
                while (requsetQueues.Count == 0 || requsetQueues.Peek().regions.Status == TaskStatus.Running);
                MyQueue queue = requsetQueues.Dequeue();
                CVRegions regions = queue.regions.Result;
                Mat frame = queue.frame;
                lock (writer)
                {
                    if (regions.predictions != null)
                    {
                        try
                        {
                            var image = drawRectOnImage(frame, regions);
                            writer.Write(image);
                        }
                        catch (ExecutionEngineException e)
                        {
                            writer.Write(frame);
                        }

                    }
                    else
                    {
                        writer.Write(frame);
                    }

                    double value = ((queue.index / totalFrames) * 100);
                    OnProgressUpdate?.Invoke(value, (int) totalFrames, (int) queue.index);
                }
                frame.Dispose();
            }

            writer.Dispose();
            OnProgressFinished.Invoke();
        }



        [STAThread]
        public async Task<bool> DetectTools(string FilePath, string newFilePath)
        {
            CustomVision customVision = new CustomVision();
            VideoCapture capture = new VideoCapture(FilePath);
            bool Reading = true;
            totalFrames = (int) Math.Floor(capture.GetCaptureProperty(CapProp.FrameCount));

             writer = new VideoWriter(newFilePath, VideoWriter.Fourcc('M', 'P', '4', 'V'), capture.GetCaptureProperty(CapProp.Fps),
                new Size(capture.Width, capture.Height), true);
            double count = 0;

            Thread thread = new Thread(() =>
            {
                makeVideo();
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();

            while (Reading)
            {
                Mat frame = capture.QueryFrame();
               
                if (frame != null)
                {
                    count++;
                    while (requsetQueues.Count == 20)
                    {
                        Thread.Sleep(100);
                    }
                    MyQueue queue = new MyQueue();
                    queue.frame = frame.Clone();
                    queue.index = count;
                    queue.regions = customVision.processImage(frame.Clone());
                    requsetQueues.Enqueue(queue);
                    
                    frame.Dispose();
                }
                else
                {
                    Reading = false;
                }
            }
            capture.Dispose();
            return true;
        }

        [STAThread]
        private Mat drawRectOnImage(Mat frame, CVRegions regions, int threshold = 18)
        {
            // Load the image (probably from your stream)

            Image<Bgr, Byte> image = frame.ToImage<Bgr, Byte>();
            
            foreach (CVRegions.Predictions prediction in regions.predictions)
            {
                if (prediction.probability * 100 < threshold)
                    continue;

                //string tagName = tagNamesExcel.getTagName(prediction.tagName) + "  " + prediction.probability * 100;
                string tagName = prediction.tagName + "  " + prediction.probability * 100;
                int x = (int) (prediction.boundingBox.left * image.Width);
                int y = (int) (prediction.boundingBox.top * image.Height);
                int width = (int) (prediction.boundingBox.width * image.Width);
                int heigth = (int) (prediction.boundingBox.height * image.Height);
                //---- insert into tb
                /*string insert_url = MClass.apiLink + "/api/Vehicle/insert/data";
                var client = new HttpClient();
                mdlVideoObjects detectObj = new mdlVideoObjects();
                detectObj.oVideoID = 854;

                var uri = new Uri(string.Format(insert_url, string.Empty));
                var json = JsonConvert.SerializeObject(detectObj);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = client.PostAsync(uri, content);
                //var content2 =  response.Content.ReadAsStringAsync();
                //InsertVehicleAttributes vehicle = JsonConvert.DeserializeObject<InsertVehicleAttributes>(content2);
            */   
            //------------------------------------------
                if (prediction.tagName == "Tool1")
                {
                    image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Orange));
                    image.Draw(tagName, new Point(x, y), FontFace.HersheyComplexSmall, 2, new Bgr(Color.Orange));
                }
                else if (prediction.tagName == "Tool2")
                {
                    image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Red));
                    image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain,2, new Bgr(Color.Red));
                }
                else if (prediction.tagName == "Tool3")
                {
                    image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Green));
                    image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 2, new Bgr(Color.Green));
                }
                else if (prediction.tagName == "Tool4")
                {
                    image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Blue));
                    image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 2, new Bgr(Color.Blue));
                }
                else if (prediction.tagName == "Tool5")
                {
                    image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.White));
                    image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 2, new Bgr(Color.White));
                }
                else if (prediction.tagName == "Tool6")
                {
                    image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Yellow));
                    image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 2, new Bgr(Color.Yellow));
                }
                /*
                                if (prediction.tagName == "DeBakey Forceps")
                                {
                                    image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Orange));
                                    image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 2, new Bgr(Color.Orange));
                                }
                                else if (prediction.tagName == "Long Bipolar Grasper")
                                {
                                    image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Red));
                                    image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 2, new Bgr(Color.Red));
                                }
                                else if (prediction.tagName == "Resano Forceps")
                                {
                                    image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Green));
                                    image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 2, new Bgr(Color.Green));
                                }
                                else if (prediction.tagName == "Suction Tool")
                                {
                                    image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Blue));
                                    image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 2, new Bgr(Color.Blue));
                                }
                                else if (prediction.tagName == "Tool 4")
                                {
                                    image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.SpringGreen));
                                    image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 2, new Bgr(Color.Red));
                                }
                                */
                /* if (prediction.tagName == "Maryland Bipolar Forceps")
                 {
                     image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Orange));
                     image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain,3 , new Bgr(Color.Orange));
                 }
                 else if(prediction.tagName == "Fenestrated Bipolar Forceps")
                 {
                     image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Red));
                     image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain,3, new Bgr(Color.Red));
                 }
                 else if (prediction.tagName == "Permanent Cautery Spatula")
                 {
                     image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.RoyalBlue));
                     image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 3, new Bgr(Color.Red));
                 }
                 else if (prediction.tagName == "ProGrasp Forceps")
                 {
                     image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.Yellow));
                     image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 3, new Bgr(Color.Red));
                 }
                 else if (prediction.tagName == "Tool 4")
                 {
                     image.Draw(new Rectangle(x, y, width, heigth), new Bgr(Color.SpringGreen));
                     image.Draw(tagName, new Point(x, y), FontFace.HersheyPlain, 3, new Bgr(Color.Red));
                 }
                 */
            }
            return image.Mat;
        }

    }

    struct MyQueue
    {
        public double index;
        public Task<CVRegions> regions { set; get; }
        public Mat frame { set; get; }
    }
}
