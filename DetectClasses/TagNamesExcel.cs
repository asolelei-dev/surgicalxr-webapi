﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webAPI.DetectClasses
{
    class TagNamesExcel
    {
        public int iter = 0;
        private Stream stream;
        private IExcelDataReader reader;
        public TagNamesExcel()
        {
            stream = File.Open("TagNames.xlsx", FileMode.Open, FileAccess.Read);
            reader = ExcelReaderFactory.CreateReader(stream);
        }

        [STAThread]
        public string getTagName(string id)
        {
            try
            {
                do
                {
                    while (reader.Read())
                    {
                        string tagId = reader.GetString(0);
                        if (tagId.Equals(id))
                        {
                            var res = reader.GetString(1);
                            reader.Reset();
                            return res;
                        }
                    }
                } while (reader.NextResult());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            reader.Reset();
            return null;
        }
    }
}
