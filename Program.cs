﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using System.Data;
using Microsoft.Azure.Management.Media;
using webAPI.MainClass;
using webAPI.Services;
using webAPI.Controllers;
using System.Net.Http;
using MediaServices = webAPI.Services.MediaServices;

namespace webAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        /*
        static async Task Main(string[] args)
        {
            // this is a big no-no in practice! never hard-code sensitive data or commit it to a repository
            // a better approach would be to use a ConfigurationBuilder instance: 
            // https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-2.1&tabs=basicconfiguration
            string tenantDomain = "azuresibelanddawn.onmicrosoft.com";
            string restApiUrl = "https://mtoolsmediaservices.restv2.southcentralus.media.azure.net/api/";
            string clientId = "585529e4-c763-4aa5-abbd-76a1bacf9172";
            string clientSecret = "cRp4fER/LqNKw01Dmog8LgWYN89w9PWSxyW+XfMyDEQ=";

            MediaServices mediaService = new MediaServices(tenantDomain, restApiUrl, clientId, clientSecret);
            await mediaService.InitializeAccessTokenAsync();

            string accessPolicyId = await mediaService.GenerateAccessPolicy("TestAccessPolicy", 100, 2);
            Asset asset = await mediaService.GenerateAsset("TestAsset", "your-storage-bucket-name");
            Locator locator = await mediaService.GenerateLocator(accessPolicyId, asset.Id, DateTime.Now.AddMinutes(-5), 1);

            FileStream fileStream = new FileStream("456.mp4", FileMode.Open);
            StreamContent content = new StreamContent(fileStream);

            // upload the file to azure and generate the asset's file info
            await mediaService.UploadBlobToLocator(content, locator, "sample-video-file.mp4");
            await mediaService.GenerateFileInfo(asset.Id);

            // run an encoding job on the uploaded asset
            string mediaProcessorId = await mediaService.GetMediaProcessorId("Media Encoder Standard");
            var result = await mediaService.CreateJob("Test Encoding Job", asset.Uri, mediaProcessorId, "H264 Multiple Bitrate 720p");
        }
        */


        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
        
    }
}
